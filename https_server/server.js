'use strict'

//引入https模块
var https = require('https');
//引入文件系统，主要作用是读取证书
var fs = require('fs');

var options = {
    //证书的key
  key  : fs.readFileSync('./cert/2498161_andy-upp.top.key'),
    //证书
  cert : fs.readFileSync('./cert/2498161_andy-upp.top.pem')
}

//通过https模块创建服务，server里面是一个匿名的回调函数，有两个参数，一个是用户请求，另一个是服务响应
//第一个参数是options，就是我们的这个证书
var app = https.createServer(options, function(req, res){
    //返回头，200表示响应成功，内容是一个文本
	res.writeHead(200, {'Content-Type': 'text/plain'});
	//响应结束，返回一个字符串
	res.end('HTTPS:Hello World!\n');

//打开侦听，侦听443端口，IP地址 0.0.0.0  表示侦听所有网卡的8080端口都提供服务
}).listen(443, '0.0.0.0');
