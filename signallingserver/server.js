'use strict'

var http = require('http');
var https = require('https');
var fs = require('fs');

var express = require('express');
var serveIndex = require('serve-index');

//socket.io
var socketIo = require('socket.io');


//引入日志依赖包
var log4js = require('log4js');

//日志配置
log4js.configure({
    appenders: {
        file: {
            type: 'file',
            filename: 'app.log',
            layout: {
                type: 'pattern',
                pattern: '%r %p - %m',
            }
        }
    },
    categories: {
        default: {
            appenders: ['file'],
            level: 'debug'
        }
    }
});

var logger = log4js.getLogger();


var app = express();
app.use(serveIndex('./public'));
app.use(express.static('./public'));

//http server
var http_server = http.createServer(app);
http_server.listen(8001, '0.0.0.0');
/*
var options = {
    key : fs.readFileSync('./cert/1557605_www.learningrtc.cn.key'),
    cert: fs.readFileSync('./cert/1557605_www.learningrtc.cn.pem')
}
*/

//https server
// var https_server = https.createServer(options, app);



//bind socket.io with https_server
//绑定后的io相当于一个站点，在这个站点里可以有很多到房间，用户链接上来后需要进入到不同的房间去
//此时https和socketio都在运行，并且两个服务都是绑定在443端口
var io = socketIo.listen(http_server);
var sockio = socketIo.listen(http_server);


//处理connection，处理io站点上的所有socket，每个socket可以认为是一个链接
//on 监听connection 事件，得到每个客户端链接socket，再去处理
io.sockets.on('connection', (socket)=>{
    socket.on('message', (room, data)=>{
        socket.to(room).emit('message', room, socket.id, data)//房间内所有人,除自己外
    });

    //该函数应该加锁
    //socket作为一个客户端，监听我们自己定义join消息，意思是加入房间，参数room为房间的名字
    socket.on('join', (room)=> {
        //将房间的名字加入 socket
        socket.join(room);

        //在rooms中获取这个房间对象
        var myRoom = io.sockets.adapter.rooms[room];
        //通过房间对象获取到房间里用户的人数
        var users = Object.keys(myRoom.sockets).length;

        logger.log('the number of user in room is: ' + users);

        //在这里可以控制进入房间的人数,现在一个房间最多 2个人
        //为了便于客户端控制，如果是多人的话，应该将目前房间里
        //人的个数当做数据下发下去。
        if(users < 3) {
            //给客户端返回消息joined，表示已经加入成功了
            socket.emit('joined', room, socket.id);
            if (users > 1) {
                //给房间里 除了自己以外的所有人回消息
                socket.to(room).emit('otherjoin', room);//除自己之外
            }
        }else {
            socket.leave(room);
            //
            socket.emit('full', room, socket.id);
        }
        //socket.to(room).emit('joined', room, socket.id);//除自己之外
        //io.in(room).emit('joined', room, socket.id)//房间内所有人
        //socket.broadcast.emit('joined', room, socket.id);//除自己，全部站点
    });

    //用户离开房间消息
    socket.on('leave', (room)=> {
        var myRoom = io.sockets.adapter.rooms[room];
        var users = Object.keys(myRoom.sockets).length;
        //users - 1;

        logger.log('the number of user in room is: ' + (users-1));

        socket.leave(room);
        socket.to(room).emit('bye', room, socket.id)//房间内所有人,除自己外
        socket.emit('leaved', room, socket.id);
        //socket.to(room).emit('joined', room, socket.id);//除自己之外
        //io.in(room).emit('joined', room, socket.id)//房间内所有人
        //socket.broadcast.emit('joined', room, socket.id);//除自己，全部站点
    });

});

/*
//connection
sockio.sockets.on('connection', (socket)=>{

    socket.on('message', (room, data)=>{
        socket.to(room).emit('message', room, socket.id, data)//房间内所有人
    });

    socket.on('join', (room)=> {
        socket.join(room);
        var myRoom = sockio.sockets.adapter.rooms[room];
        var users = Object.keys(myRoom.sockets).length;
        logger.log('the number of user in room is: ' + users);
        socket.emit('joined', room, socket.id);
        //socket.to(room).emit('joined', room, socket.id);//除自己之外
        //io.in(room).emit('joined', room, socket.id)//房间内所有人
        //socket.broadcast.emit('joined', room, socket.id);//除自己，全部站点
    });

    socket.on('leave', (room)=> {
        var myRoom = sockio.sockets.adapter.rooms[room];
        var users = Object.keys(myRoom.sockets).length;
        //users - 1;

        logger.log('the number of user in room is: ' + (users-1));

        socket.leave(room);
        socket.emit('leaved', room, socket.id);
        //socket.to(room).emit('joined', room, socket.id);//除自己之外
        //io.in(room).emit('joined', room, socket.id)//房间内所有人
        //socket.broadcast.emit('joined', room, socket.id);//除自己，全部站点
    });
});
*/

//https_server.listen(443, '0.0.0.0');




