
'use strict'

//获取select这个元素中id为audioSource的元素
var audioSource = document.querySelector("select#audioSource");
var audioOutput = document.querySelector("select#audioOutput");
var videoSource = document.querySelector("select#videoSource");
//filter
var filtersSelect = document.querySelector("select#filter");


//picture
var snapshot = document.querySelector('button#snapshot');
var picture = document.querySelector('canvas#picture');
picture.width = 320;
picture.height = 240;


//获取音频
//var audioplay = document.querySelector('audio#audioplayer')


//首先获取到在html中定义的vedio标签，从而获取视频
var videoplay = document.querySelector('video#player');

//获取到 divConstraints
var divConstraints = document.querySelector('div#constraints');

//获取设备信息，参数是设备信息的数组
function gotDevices(deviceInfos) {
    //遍历设备信息数组
    deviceInfos.forEach(
        //拿到每一项的deviceInfo作为参数
       function (deviceinfo) {
           //select 中的每一项是一个option，根据不同的种类添加到不同的select中去
           var option = document.createElement('option');
           option.text = deviceinfo.label;  //设备名称
           option.value = deviceinfo.deviceId //值是deviceid
           console.log("deviceinfo.kind:",deviceinfo.kind);
           if(deviceinfo.kind === 'audioinput'){
               audioSource.appendChild(option);
           }else if(deviceinfo.kind === 'audiooutput'){
               audioOutput.appendChild(option);
           }else if(deviceinfo.kind === 'videoinput'){
               videoSource.appendChild(option);
           }
       }
    )
}


//实现获取流之后的方法,将获取到的流赋值给我们在html中定义的vedio标签
function gotMediaStream(stream) {
    //指定标签获取视频流的数据源
    videoplay.srcObject = stream;
    //通过media stream 拿到视频的track，视频轨,只取第一个
    var videoTrack = stream.getVideoTracks()[0];
    //拿到vedio的所有约束
    var videoConstraints = videoTrack.getSettings();
    //将其转成json格式，作为内容赋值给divConstraints的内容
    divConstraints.textContent =   JSON.stringify(videoConstraints,null,2);

    //audioplay.srcObject = stream;

    //拿到流之后，说明用户已经同意访问音视频设备了，此时可以返回一个promise，获取
    //所有的音视频设备
    return navigator.mediaDevices.enumerateDevices();
}

//获取失败 打印出错信息
function handleError(err) {
    console.log('getUserMedia error:', err);
}


function start() {

//如果这个方法不存在，则打印
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        console.log('getUserMedia is not supported！')
        return;
    } else {

        var deviceId = videoSource.value;

        //设置参数，采集音视频，并设置视频参数和音频参数
        var constraints = {
            video: {
                width: 320,
                height: 240,
                frameRate: 30,
                facingMode: 'enviroment',
                //判断deviceId的值是否为空，如果不为空就设置它的值为deviceId，如果为空就设置为undefined
                deviceId: deviceId ? deviceId: undefined
            },
            // vedio:false,
            audio: {
                noiseSuppression: true,
                echoCancellation: true
            },

        }

        //如果存在，就调用这个方法，成功进入成功方法，失败进入失败方法
        //成功之后，由于promise是可以串联的，then之后可以继续then
        navigator.mediaDevices.getUserMedia(constraints)
            .then(gotMediaStream)
            .then(gotDevices)
            .catch(handleError);
    }
}

start();


//增加事件，选择摄像头的时候重新调用start函数,实现设备切换
videoSource.onchange = start;
//增加事件处理，当我们选择其中的某一项变化的时候，进行处理
//onchange 事件会在域的内容改变时发生
filtersSelect.onchange = function () {
    //获取视频的模式的名字
    videoplay.className = filtersSelect.value;

}

//给snapshot添加onclick事件，当我们点击button的时候，触发这个事件
snapshot.onclick =  function () {
    //设置滤镜
    picture.className = filtersSelect.value;
    //得到picture的上下文,将videoplay当成源，从这里截取一帧数据，写成一张图片
    picture.getContext('2d').drawImage(videoplay,
                                         0,0,
                                         picture.width,
                                         picture.height);
}

















