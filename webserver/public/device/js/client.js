'use strict'

//获取select这个元素中id为audioSource的元素
var audioSource = document.querySelector("select#audioSource");
var audioOutput = document.querySelector("select#audioOutput");
var videoSource = document.querySelector("select#videoSource");



// 如果浏览区不支持，打印信息
if(!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices){
    console.log('emumerateDevices is not supported！');
}else {
    //如果调用成功，可以直接调用then方法，因为本身这个对象也是一个promise
    //then方法中传一个参数，就是我们自己的函数,如果出错了就catch,调用出错函数
    navigator.mediaDevices.enumerateDevices()
        .then(gotDevices)
        .catch(handleError);
}

function gotDevices(deviceInfos) {
       //当我们拿到deviceInfo这个数组之后，就遍历这个数组中的每一项
       //而在每一项中我们又可以注册一个匿名函数去处理每一项的内容，参数就是数组中的每一项
        deviceInfos.forEach(function (deviceInfo) {
            //打印设备信息
            console.log(deviceInfo.kind + "label = "
                     + deviceInfo.label + ":id = "
                     + deviceInfo.deviceId + ": groupId ="
                     + deviceInfo.groupId);

            //创建option元素
            var option = document.createElement('option');
            option.text = deviceInfo.label;
            option.value = deviceInfo.deviceId;

            //将设备信息添加到不同的元素中去
            if(deviceInfo.kind === 'audioinput'){
                  audioSource.appendChild(option);
            }else if(deviceInfo.kind === 'audiooutput'){
                  audioOutput.appendChild(option);
            }else if(deviceInfo.kind === 'videoinput'){
                  videoSource.appendChild(option);
            }
        })
}

function handleError(err) {
    console.log(err.name + " : " + err.message);
}













