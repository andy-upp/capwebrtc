'use strict'

var http = require('http');
var https = require('https');
var fs = require('fs');


var express = require('express');
var serveIndex = require('serve-index');



//实例化express模块
var app = express();
app.use(serveIndex('./public'));
//发布指定的静态目录作为发布路径
app.use(express.static('./public'));


//http server  监听80端口
var http_server = http.createServer(app);
http_server.listen(80, '0.0.0.0');


var options = {
	key : fs.readFileSync('./cert/2498161_andy-upp.top.key'),
	cert: fs.readFileSync('./cert/2498161_andy-upp.top.pem')
}

//https server
var https_server = https.createServer(options, app);

https_server.listen(443, '0.0.0.0');




